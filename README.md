# 418 I'm a Teapot - Crazy Charly Day

Laravel 5.5

Dépendances :

+ php >=7.0.0

+ composer

+ npm (pour compiler les ressources, voir [Laravel Mix](https://laravel.com/docs/5.7/mix))


Configuration nécessaire (depuis le répertoire site/) :

0. `composer install`


0. `cp .env.example .env`


0. Modifier le fichier .env avec sa config locale (pour la base de données)


0. Lancer les migrations : `php artisan migrate`


0. Lancer un serveur (on peut utiliser `php artisan serve`)


Pour compiler les feuilles de styles SASS et le JS:

0. Installer les dépendances de node : `npm install`

0. Lancer Laravel Mix : `npm run dev` ou `npm run watch` (pour suivre les changements automatiquement)
