# Liste des fonctionnalités implémentées :

* Candidatures :
  * Création d'une candidature
  * Liste des candidatures pour un employeur
  * Liste des candidatures pour un candidat
  * Accès à une candidature pour un candidat
  
 12.Candidature à une offre : la candidature est automatiquement positionnée dans
l’état « en attente ». La même page permet également de définir un besoin de
transport pour cette candidature : le candidat choisit le type de véhicule (pas de
besoin de transport, véhicule normal ou adapté), il saisit au besoin son lieu de
départ à partir d’une zone de saisie libre qui peut également être alimentée par
l’adresse même du candidat (proposée pour un remplissage automatique). Le
point d’arrivée correspond forcément à l’adresse de l’offre correspondante.
4
13.Listes des candidatures pour un candidat : chaque utilisateur connecté peut
afficher la liste de ses candidatures. Chaque candidature est associée à un lien
permettant d’avoir les détails de cette candidature.
14.Listes des candidatures pour un employeur : chaque employeur connecté peut
afficher la liste des candidatures sur ses offres d’emploi. Chaque candidature est
associée à un lien permettant d’avoir les détails de cette candidature.
15.Affichage et modification d’une candidature pour un candidat : à partir de
l’affichage détaillé d’une candidature, un utilisateur peut éditer (changement de
lieu s’il n’y a pas de proposition de transport) ou supprimer cette candidature.
16.Affichage et modification d’une candidature pour un employeur : à partir de
l’affichage détaillé d’une candidature, un employeur peut changer l’état de cette
candidature. Attention : si une candidature est acceptée, l’état ne peut plus être
changée.

 
 
* Offres d'emploi :
Création d’une offre d’emploi : une page permet d’ajouter une offre d’emploi sur
le site. Un utilisateur doit être connecté pour réaliser cette opération : il sera
automatiquement l’employeur de l’offre.
8. Affichage de la liste des offres d’emploi : les offres d’emploi sont présentées en
affichant synthétiquement leurs caractéristiques. Les offres sont triées par défaut
de façon anti-chronologique (la dernière postée apparaît en premier). Chaque
offre possède un lien permettant d’accéder aux informations détaillées de l’offre.
La présentation détaillée d’une offre propose des actions : candidater à une offre
(voir fonctionnalité ci-après) et, de façon optionnelle, mettre l’offre dans les
favoris de l’utilisateur.
9. Affichage d’une carte des offres disponibles (avancé) : présentation des offres
sur une carte géographique. Un clic sur une offre permet d’accéder à la
description détaillée de l’offre décrite dans la fonctionnalité précédente.
10.Recherche d’une offre d’emploi (avancé) : propose plusieurs champs permettant
de filtrer les offres d’emploi recherchées. La validation de ces critères mène à
une liste d’offres d’emploi (voir fonctionnalité ci-dessus) restreintes à celles
répondant aux critères choisis.
  * Affichage d'une offre d'emploi
  * Liste des candidatures pour un candidat
  * Liste des candidatures pour un employeur
  * Création et modification d'une candidature pour un employeur
  * Recherche d'une offre d'emploi (avancé)

* Transports :
  * Création d'un besoin de transport via une candidature
  * Gestion des besoins de transport
  * Gestion des propositions de transport pour un chauffeur
  * Gestion des propositions de transport pour un employé
  
* Compte :

2. Création d’un nouveau compte (avancé) : l’utilisateur peut créer un nouveau
compte pour s’enregistrer. Il doit alors saisir un login (unique sur le site) et un
mot de passe.
3. Accès avec authentification (avancé) : l’utilisateur doit fournir un login et un mot
de passe pour s’authentifier dans l’application et accéder à son compte. Il doit
également pouvoir se déconnecter.
4. Modification de compte (avancé) : l’utilisateur peut modifier les informations
relatives à son compte s’il s’est connecté au préalable.
6. Compte administrateur (avancé) : certains comptes ont des privilèges
d’administration sur le site, ce qui leur permet d’accéder au back-office et de
réaliser certaines opérations décrites ci-dessous. L’affichage détaillé d’un
utilisateur par un administrateur permet d’accéder à l’ajout de l’utilisateur affiché
comme nouvel administrateur.
(/admin) compte: admin@admin.com mdp:123456

* Seeder (Génération des données) pour toutes les tables
  

# Liste des fonctionnalités partiellement-implémentées (uniquement en back-end):

une page covoiturage est partiellement accessible (que par url)
/Covoiturage
Elle liste les personnes qui ont besoins d'un transport

# Liste des fonctionnalités non-implémentées :

* Offres d'emploi :
  * Accès à la liste des offres favorites (avancé)
  * [SUPERBONUS] Gestion des dates (début / fin) pour les offres d'emploi

* Transports :
  * Alerte sur les candidatures pour les besoins de transport