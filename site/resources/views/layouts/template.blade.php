<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="418 I'm a teampot">
    <title>{{$titre ?? "Handi Intérim"}}</title>

<link href="{{ url('/')}}/css/bootstrap.css" rel="stylesheet">
@section('style')
<link href="{{ url('/')}}/css/main.css" rel="stylesheet">
@show

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
  </head>

  <body>
    <nav class="header navbar navbar-expand-md navbar-dark bg-dark mb-4">
  <a class="navbar-brand" href="{{route('accueil')}}">Handi Itérim</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item {{Route::currentRouteName() === 'accueil' ? "active" : ""}}">
        <a class="nav-link" href="{{route('accueil')}}">Accueil</a>
      </li>
      <li class="nav-item {{Route::currentRouteName() === 'offresDispos' ? "active" : ""}}">
          <a class="nav-link" href="{{ route('offresDispos') }}">Offres d'emploi</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cartes
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{route('Carte','Emploi')}}">Emploi</a>
          <a class="dropdown-item" href="{{route('Carte','Covoiturage')}}">Demande de transport</a>
      </li>
      @guest
          <li class="nav-item {{Route::currentRouteName() === 'login' ? "active" : ""}}">
              <a class="nav-link" href="{{ route('login') }}">Se connecter</a>
          </li>
      @else
        <li class="nav-item {{Route::currentRouteName() === 'compte' ? "active" : ""}}">
          <a class="nav-link" href="{{route('compte')}}">Mon compte</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('rechercher')}}">Rechercher</a>
        </li>
        <li class="nav-item {{Route::currentRouteName() === 'notifications' ? "active" : ""}}">
          <a class="nav-link" href="{{route('notifications')}}">Notifications</a>
        </li>
      @endguest
    </ul>
    <!-- <form class="form-inline mt-2 mt-md-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
      <button class="btn  my-2 my-sm-0" type="submit">Search</button>
  </form> -->
  </div>
</nav>
@yield('image')

<main role="main" class="ctnPrincipal container">

@yield('contenu')

</main>
@yield('nonImp')

<footer class="">
<p>Crazy Charly Day</p>
<p>418 i'm a teapot</p>
</footer>
<script src="{{ url('/')}}/js/jquerry.js" ></script>
<script src="{{ url('/')}}/js/bootstrap.bundle.min.js" ></script></body>
@yield('js')
</html>
