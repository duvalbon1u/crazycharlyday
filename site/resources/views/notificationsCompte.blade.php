@extends('layouts.template')
@section('style')
    @parent
<link href="{{ url('/')}}/css/notification.css" rel="stylesheet">
@endsection

@section('contenu')
<div class="row">
@if(count($candidature)!=0)
<h1 >Statut de vos candidatures :</h1>
<ul>
@foreach($candidature as $candid)
<div class="col-12">
    <li>
        <a href="{{route('offre',['idOffre' => $candid->offre_id])}}"><h3>{{$candid->offre->intitule}}</h3></a>
    <p>{{$candid->offre->profilPosteCourt}}</p>
    <p> Etat : {{$candid->etat}}</p>
    </li>
</div>
@endforeach
</ul>
@else
    <h1 class="col">Vous n'avez pas de candidatures</h1>
@endif
</div>
<div class="separation"></div>

<div class="row">
@if(count($offreEmple)!=0)
    <h1 class="col-12">Les offres d'emploi que vous avez crée :</h1>
@foreach($offreEmple as $offre)
    <div class="col-md-6 col-sm-12">
    <div class="col-12">
        <a href="{{route('offre',['idOffre'=>$offre->id])}}"><h1 class="col">{{$offre->intitule}}</h1></a>
        <p class="col-sm-12 col-md-6">Etat : {{$offre->etat}}</p>
        <p class="col-sm-12 col-md-6">Date expiration : {{$offre->date_expiration}}</p>
        <a href="{{route('listeCandi',['idOffre'=>$offre->id])}}"><button class="col-md-8 col-sm-12 btn btn-success">Consulter les réponses</button></a>
        <a href="{{route('modifOffre',['idOffre'=>$offre->id])}}"><button class="col-md-8 col-sm-12 btn btn-warning">Modifier l'offre</button></a>
    </div>
    </div>
@endforeach
@else
    <h1 class="col">Vous n'avez pas crée d'offres d'emploi</h1>
@endif
</div>
@endsection