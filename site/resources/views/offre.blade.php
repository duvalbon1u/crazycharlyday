@extends('layouts.template')
@section('style')
    @parent
<link href="{{ url('/')}}/css/offre.css" rel="stylesheet">
<link href="{{ url('/')}}/css/carte.css" rel="stylesheet">
<link rel="stylesheet" href="{{ url('/')}}/js/leaflet/leaflet.css" />
@endsection
@section('contenu')
<div class="row">
    <h1 class="col-12">{{$offre->intitule . "[". $offre->categorie->nom ."]"}}</h1>
    <h2 class="col-12">Cette offre est proposé par : {{$offre->employeur->nom . " ". $offre->employeur->prenom}}</h2>
    <p class="col-12">{{$offre->profilPoste}}</p>

    @if($offre->etat == "actif")
        <h3 class="col-12">Expire le {{$offre->date_expiration->formatLocalized('%A %d %B %Y')}}</h3>
        <div class ="col-md-6 col-sm-12 ok">
            <h3>L'annonce est disponible</h3>
            <a href={{route('candidature',['idOffre'=>$offre->id])}}><button class="btn btn-success">Candidater</button></a>
    </div>
    @else
        <div class ="col-md-6 col-sm-12 pasOk"><h3>L'annonce n'est plus disponible</h3></div>
    @endif
    <div class="col-md-6 col-sm-12">
        <h3 class="col-12 pt-3" id ="titreCarte">Position de l'entreprise</h3>
        <p class="col-12">Adresse : {{$offre->lieu}}  </p>
    </div>
    <div id="map" data-fetch-url="{{route('offre.JSON',array('Emploi', $offre->id))}}">
    </div>

</div>
@endsection
@section('js')
  @parent
<script src="{{ url('/')}}/js/leaflet/leaflet.js"></script>
<script src='{{ url('/')}}/js/carte.js'></script>
@endsection
