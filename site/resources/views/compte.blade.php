@extends('layouts.template')
@section('style')
    @parent
<link href="css/compteCss.css" rel="stylesheet">
@endsection

@section('contenu')
<h1>Mon Compte</h1>

  <div class="row">
      <div class="col-md-3">
          <div class="description col-md-6 shadow-lg p-3 mb-5 rounded">
            <img src="{{ url('/')}}/img/{{$usr->avatar}}.jpg" alt="photo de profil de {{$usr->prenom." ".$usr->nom}}">
            <div class="trait col-md-12"></div>
            <div class="col-md-12">Prenom : {{$usr->prenom}}</div>
            <div class="trait col-md-12"></div>
            <div class="col-md-12">Nom : {{$usr->nom}}</div>
            <div class="trait col-md-12"></div>
            <a href={{route('ParametreCompte')}}>
              <div class="col-md-12">parametre du compte</div>
            </a>
            <div class="trait col-md-12"></div>
          </div>
          <div class="publique col-md-6 shadow-lg p-3 mb-5 rounded" >
            <div class="col-md-12">
              <a href={{route('ComptePublique')}}>
                <p>voir page version publique</p>
              </a>
            </div>
          </div>
        </div>

        <div class="col-md-9">
          <div class="row">
            @foreach($nombres as $temp)
            <div class="article col-md-12  shadow-lg p-3 mb-5 bg-white rounded" >
              <p>article {{$temp}}</p>
            </div>
            @endForeach
          <div>
        </div>
    </div>
@endsection
