@extends('layouts.template')
@section('style')
    @parent
<link href="/css/recherche.css" rel="stylesheet">
@endsection

@section('contenu')
<h1>Effectuer une recherche sur l'intitulé d'une offre d'emploi</h1>
<form class="form-group" method="get">
    <input name="search" type="search" class="input-sm form-control" placeholder="Recherche">
    
    <button type="submit" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-eye-open"></span> Chercher</button>
</form>
<div class="row">
@if(count($res)==0)
    <h3>Rien ne correspond à votre recherche</h3>
@else
@foreach($res as $temp)
<div class="col-md-6 col-sm-12">
    <a href="{{route('offre',['idOffre'=>$temp->id])}}"><h3>{{$temp->intitule}}</h3></a>
    <p> Etat {{$temp->etat}}</p>
    </div>
@endforeach
@endif
</div>
    @endsection