@extends('layouts.template')

@section('contenu')
    <div class="jumbotron">
    <h1>Navbar example</h1>
    <p class="lead">This example is a quick exercise to illustrate how the top-aligned navbar works. As you scroll, this navbar remains in its original position and moves with the rest of the page.</p>
    <a class="btn btn-lg btn-primary" href="" role="button">View navbar docs &raquo;</a>
  </div>

    @if(count($nombres) == 0)
        <h1>C'est vide!!</h1>
    @endif
    @foreach($nombres as $temp)
        <div class="jumbotron">
        <h1>Navbar example {{$temp}}</h1>
        <p class="lead">This example is a quick exercise to illustrate how the top-aligned navbar works. As you scroll, this navbar remains in its original position and moves with the rest of the page.</p>
        <a class="btn btn-lg btn-primary" href="" role="button">View navbar docs &raquo;</a>
      </div>
    @endForeach
@endsection