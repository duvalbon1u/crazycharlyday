@extends('layouts.template')
@section('style')
    @parent
<link href="{{ url('/')}}/css/creationOffre.css" rel="stylesheet">
@endsection
@section('contenu')
<h1></h1>
<h1>Modification de votre offre d'emploi</h1>
<form class="form-group row" method="post">
     {{ csrf_field() }}
    <div class="col-sm-12 col-md-6">
        <label for="input7" class="col-form-label">Date</label>
        <input class="form-control" type="date" value="{{$offre->date_expiration->format("Y-m-d")}}">
    </div>
    <div class="col-sm-12 col-md-6">
        <label for="input6" class=" col-form-label">Catégorie</label>
        <div class="form-group">
        <select name="categorie" class="custom-select">
            <option selected="selected" value="{{$offre->categorie->id}}">{{$offre->categorie->nom}}</option>
             @foreach($categories as $temp)
                <option value="{{$temp->id}}">{{$temp->nom}}</option>
            @endforeach    
        </select>
    </div>
  </div>
    <div class="col-sm-12 col-md-6">
    <label for="input1" class=" col-form-label">Adresse</label>
      <input  name="lieu" type="text" class="form-control" id="input1" placeholder="{{$offre->lieu}}">
    </div>
    <div class="col-sm-12 col-md-6">
    <label for="input2" class=" col-form-label">Intitule</label>
      <input name = "intitule" type="text" class="form-control" id="input2" placeholder="{{$offre->intitule}}">
    </div>
    <div class="col-sm-12 col-md-6">
    <label for="input3" class=" col-form-label">Description rapide</label>
      <textarea class="form-control" name="profilPosteCourt" rows="10" cols="30">{{$offre->profilPosteCourt}}</textarea>
    </div>
    <div class="col-sm-12 col-md-6">
    <label for="input4" class=" col-form-label">Description longue</label>
        <textarea class="form-control" name="profilPoste" rows="10" cols="30">{{$offre->profilPoste}}</textarea>
    </div>
    <div class="col-sm-12 col-md-6">
    <button class="btn btn-success col-md-6 col-sm-6" type="submit">Modifier votre offre d'emploi</button>
    </div>
</form>
@endsection