@extends('layouts.template')

@section('style')
@parent
<link href="{{ url('/')}}/css/covoit.css" rel="stylesheet">
@endsection
@section('contenu')
<div class="présentation">

  <div class="input-group">
    <input type="text" class="form-control" placeholder="Rechercher">
    <div class="input-group-append">
      <button class="btn btn-info" type="button" id="button-addon2">Rechercher</button>
      <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Trier par</button>
    </div>
  </div>
  <div class="gauche">
  @foreach($covoiturage as $cov)
  <section>
    <p>Nom : {{$cov->nom}} - Prénom : {{$cov->prenom}}<p>
      <p>Adresse perso : {{$cov->adressePerso}}</p>
  </section>
  @endforeach
@endsection
