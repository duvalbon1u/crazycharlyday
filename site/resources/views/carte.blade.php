@extends('layouts.template')

@section('style')
@parent
<link rel="stylesheet" href="{{ url('/')}}/js/leaflet/leaflet.css" />
@endsection
@section('contenu')
<div id="map" data-fetch-url="{{route('offre.JSON',$type)}}" style="height:400px;">
</div>
@endsection

@section('js')
  @parent
<script src="{{ url('/')}}/js/leaflet/leaflet.js"></script>
<script src='{{ url('/')}}/js/carte.js'></script>
@endsection
