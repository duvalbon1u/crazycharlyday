@extends('layouts.template')
@section('style')
    @parent
<link href="css/compteCss.css" rel="stylesheet">
@endsection

@section('contenu')
<div id="fond1">
  <h1>Parametre de mon Compte</h1>
    <div class="row">
      <div class=" col-md-3">
        <img src="{{ url('/')}}/img/{{$usr->avatar}}.jpg" alt="photo de profil de {{$usr->prenom." ".$usr->nom}}">
      </div>
      <div class=" col-md-3">
      </div>
      <div class=" col-md-5">
        <button id=b2 type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">modifier image</button>
        <!-- creation du modal -->
        <div class="modal fade" id="myModal" role="dialog">
          <!-- boite de dialogue -->
         <div class="modal-dialog">

           <!-- contenu -->
           <div class="modal-content">
             <div class="modal-header">
               <h4 class="modal-title">choix d'image</h4>
             </div>
             <div class="modal-body">

               <div class="row">

               @foreach($tab as $temp)
                  <div class=img1>
                     <a href="ParametreCompte{{$temp}}">
                       <img src="{{$temp}}"  style="cursor:pointer;" />
                    </a>
                  </div>
               @endforeach

              </div>
              </div>
             <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             </div>
           </div>

         </div>
        </div>


      </div>
      <div class="col-md-10">
        <form class="form" action="{{route('controleParametre')}}" method="post">
            {{ csrf_field() }}
          <!---->
          <div class="row">
            <div class="col-md-5">Prenom:
            </div>
            <div class="form-group col-md-7">
              {{$usr->prenom}}
              <input type="text" class="form-control" name="usrPrenom" placeholder={{$usr->prenom}}>
            </div>
            <div class="col-md-9"></div>
          </div>
          <div class="trait col-md-12"></div>

          <!---->
          <div class="row">
            <div class="col-md-5">Nom:
            </div>
            <div class="form-group col-md-7">
              {{$usr->nom}}
              <input type="text" class="form-control" name="usrNom" placeholder={{$usr->nom}} >
            </div>
            <div class="col-md-9"></div>
          </div>
          <div class="trait col-md-12"></div>

          <!---->
          <div class="row">
            <div class="col-md-5">email:
            </div>
            <div class="form-group col-md-7">
              {{$usr->email}}
              <input type="text" class="form-control" name="usrEmail" placeholder={{$usr->email}} >
            </div>
            <div class="col-md-9"></div>
          </div>
          <div class="trait col-md-12"></div>

          <!---->
          <div class="row">
            <div class="col-md-5">adresse Professionnel:
            </div>
            <div class="form-group col-md-7">
              {{$usr->adressePro}}
              <input type="text" class="form-control" name="usrAdressePro" placeholder={{$usr->adressePro}}>
            </div>
            <div class="col-md-9"></div>
          </div>
          <div class="trait col-md-12"></div>

          <!---->
          <div class="row">
            <div class="col-md-5">adresse Personnel:
            </div>
            <div class="form-group col-md-7">
              {{$usr->adressePerso}}
              <input type="text" class="form-control" name="usrAdressePerso" placeholder={{$usr->adressePerso}}>
            </div>
            <div class="col-md-9"></div>
          </div>
          <div class="trait col-md-12"></div>
        <!---->
        </div>
      </div>
      <div class="row">
        <div class="col-md-6  "></div>
        <button id=b1 type="submit" class="btn btn-primary" value="submit">enregistrer</button>
      </div>
    </form>
  </div>
</div>
@endsection
