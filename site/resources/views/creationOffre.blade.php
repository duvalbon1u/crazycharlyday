@extends('layouts.template')
@section('style')
    @parent
<link href="{{ url('/')}}/css/creationOffre.css" rel="stylesheet">
@endsection

@section('contenu')
<h1>Creation d'une offre d'emploi à votre nom</h1>
<form class="form-group row" method="post" action="">
    {{ csrf_field() }}
    <div class="col-sm-12 col-md-6">
        <label for="input7" class="col-form-label">Date</label>
        <input name="expiration" class="form-control" type="date" required>
    </div>
    <div class="col-sm-12 col-md-6">
        <label for="input6" class=" col-form-label">Catégorie</label>
        <div class="form-group">
        <select name="categorie" class="custom-select" required>
          @foreach($categories as $temp)
            <option value="{{$temp->id}}">{{$temp->nom}}</option>
          @endforeach
        </select>
    </div>
  </div>
    <div class="col-sm-12 col-md-6">
    <label for="input1" class=" col-form-label">Adresse</label>
      <input  name="lieu" type="text" class="form-control" id="input1" placeholder="Lieu de travail" required>
    </div>
    <div class="col-sm-12 col-md-6">
    <label for="input2" class=" col-form-label">Intitule</label>
      <input name = "intitule" type="text" class="form-control" id="input2" placeholder="Intitulé du métier" required>
    </div>
    <div class="col-sm-12 col-md-6">
    <label for="input3" class=" col-form-label">Description rapide</label>
      <textarea class="form-control" name="profilPosteCourt" rows="10" cols="30" required></textarea>
    </div>
    <div class="col-sm-12 col-md-6">
    <label for="input4" class=" col-form-label">Description longue</label>
        <textarea class="form-control" name="profilPoste" rows="10" cols="30" required></textarea>
    </div>
    <div class="col-sm-12 col-md-6">
    <button class="btn btn-success col-md-6 col-sm-6" type="submit">Publier votre offre d'emploi</button>
    </div>
</form>

@endsection
