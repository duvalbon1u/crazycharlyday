@extends('layouts.template')
@section('style')
    @parent
<link href="css/rechercheOffre.css" rel="stylesheet">
@endsection
@section('contenu')
<div class="row">
<h1 class="col-md-8 col-sm-12">Offres d'emplois</h1>
<a class="ml-auto mr-auto col-md-4 col-sm-6"href="{{route('creerOffre')}}"><button class="btn btn-custom">Créer une offre d'emploi</button></a>
</div>
<div class="contenu row">
        @foreach($offres as $temp)

        <div  class="p-2 col-sm-12 col-md-6 d-flex ">
            <div class="offres w-100">
            <a href={{route('offre', ['idOffre'=> $temp->id])}}>
                <h2>{{$temp->intitule}}</h2></a>
            <p class="entreprise">Par : {{$temp->employeur->nom . " " . $temp->employeur->prenom}}</p>
            <p>{{$temp->profilPosteCourt}}</p>
            </div>
        </div>
    @endForeach
</div>
@endsection
