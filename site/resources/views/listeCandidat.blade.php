@extends('layouts.template')
@section('style')
    @parent

@endsection

@section('contenu')
    @if(count($offres) == 0)
        <h1>Personne n'a encore répondu à votre annonce</h1>
    @else
    <div class="row">
        <h1>Les personnes qui ont répondus à votre offre :</h1>
        @foreach($offres as $temp)
            <div class="col-md-6 col-sm-12">
                <h2>{{$temp->user->prenom. " " . $temp->user->nom}}</h2>
                <p>Etat : {{$temp->etat}}</p>
                <p>A postulé le : {{$temp->created_at->formatLocalized('%A %d %B %Y')}}</p>
                @if($temp->etat == "attente")
                <form method="post" action="">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{$temp->user->id}}" name=idUser>
                    <button type="submit" name="etat" value="acceptee"  class="btn btn-success col-sm-12 col-md-6">Valider</button>
                </form>
                <form method="post" action="">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{$temp->user->id}}" name=idUser>
                    <button name="etat" value="refusee" class="btn btn-danger col-sm-12 col-md-6">Refuer</button>
                </form>
                @elseif($temp->etat=="refusee")
                    <p>Vous avez refusé cette demande</p>
                @else
                    <p>Vous avez acceptée la demande</p>
                @endif
            </div>        
        @endforeach
    </div>
    @endif
@endsection