@extends('layouts.template')

@section('style')
    @parent
    <link rel='stylesheet' href='{{ url('/')}}/css/admin.css'>
@endsection

@section('contenu')
<h1>Bonjour {{$user->prenom}}</h1>

@if(session("erreur"))
    <div class='alert alert-danger w-100 text-center'>
        {{session("erreur")}}
    </div>
@elseif(session("message"))
    <div class='alert alert-success w-100 text-center'>
        {{session("message")}}
    </div>
@endif

<h2 class='mt-5'>Statistiques du site</h2>
  <div class="row py-5">
      <div class="col-12 col-lg-6">
          <canvas id='offres-candidatures' data-fetch-url="{{route('admin.stats.candidatures')}}"></canvas>
      </div>
      <div class="col-12 col-lg-6" style='position:relative'>
          <div id='numberStats' data-fetch-url="{{route('admin.stats.numbers')}}" class='row'>
              <div class='col-4 col-md-4'><div id='nbUsers' class='number'>0</div>utilisateurs</div>
              <div class='col-4 col-md-4'><div class='number' id='nbCandidatures'>0</div>candidatures</div>
              <div class='col-4 col-md-4'><div class='number' id='nbOffresEmplois'>0</div>offres d'emploi</div>
              <div class='col-4 col-md-4'><div class='number' id='nbCandidaturesAcceptees'>0</div>emplois trouvés</div>
              <div class='col-4 col-md-4'><div class='number' id='nbCovoiturage'>0</div>covoiturages</div>
              <div class='col-4 col-md-4'><div class='number' id='nbEmployeurs'>0</div>employeurs</div>
          </div>
      </div>
    </div>

    <h2 class='mt-5'>Modification des catégories d'activité</h2>
     <div class="row py-5">
         <div class='table-responsive'>
             <table class='table w-100'>
                 <thead>
                     <tr>
                         <th>Nom</th>
                         <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $cat)
                        <tr>
                            <td>{{$cat->nom}}</td>
                            <td>
                                <form action='{{route("admin.categories.destroy", $cat)}}' method='post'>
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button class='btn btn-danger'>&times</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class='col-12'>
            <h3 class='my-5'>Ajouter une catégorie</h3>
            <form method='POST' class='form-row' action='{{route("admin.categories.store")}}'>
                {{csrf_field()}}
                <div class='col-12 col-sm-9'>
                    <input class='form-control' type='text' name='nom' placeholder='Nom de la catégorie'>
                </div>
                <div class='col-12 col-sm-3'>
                    <button class='btn btn-primary'>Ajouter</button>
                </div>
            </form>
        </div>
     </div>


@endsection

@section('js')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js" integrity="sha256-oSgtFCCmHWRPQ/JmR4OoZ3Xke1Pw4v50uh6pLcu+fIc=" crossorigin="anonymous"></script>
    <script src="{{ url('/')}}/js/admin.js"></script>

@endsection
