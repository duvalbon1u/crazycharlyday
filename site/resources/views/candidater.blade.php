@extends('layouts.template')
@section('style')
    @parent
<link href="{{ url('/')}}/css/candidater.css" rel="stylesheet">
@endsection

@section('contenu')
<div class="contenu row">
    <h1>Candidater pour l'annonce</h1>
    <h2>Rappel de l'offre :</h2>
    <div class="compact">
    <div class="offres col-12">
            <a href={{route('offre',['idOffre'=>$offre->id])}}><h3>{{$offre->intitule}}</h3></a>
        <div class="row">
        <div class=" col-sm-12 col-md-4">
            <p class="entreprise">{{$offre->employeur->nom ." ". $offre->employeur->prenom}}</p>
        </div>
        <div class=" col-sm-12 col-md-8">
            <p>{{$offre->profilPosteCourt}}</p>
        </div>
        </div>
        </div>
    </div>
</div>
<h3>Documents et informations nécessaires offre : </h3>
    <form method="post" action="{{route('candidaturePost',['idOffre'=>$offre->id])}}">
        {{ csrf_field() }}
         <input type="hidden" id ="valeurCompte" name="adresseUser" value="{{$adresse}}">

         <div class="form-group row">
             <div class="col-sm-2">
            <label id="formPerso" for="colFormLabel" class="col-form-label">Adresse de départ</label>
             </div>
             <div class="col-sm-4">
            <div class="custom-control custom-radio">
                  <input type="radio" id="customRadio4" name="profil" value="profil" class="custom-control-input" checked>
                  <label id="add" class="custom-control-label" for="customRadio4">Mon adresse </label>
            </div>
                <div class="custom-control custom-radio">
                  <input type="radio" id="customRadio5" name="lieu" value="perso" class="custom-control-input">
                  <label class="custom-control-label" for="customRadio5">Adresse personnalisé</label>

            </div>
        </div>
            <div class="col-sm-6">
              <input name="adressePerso" type="text" class="form-control" id="adresse" placeholder="" readonly>
            </div>
          </div>


        <div class="row">
        <legend class="col-form-label col-sm-2 pt-0">Mode de transport</legend>
       <div class="col-sm-10">
        <div class="custom-control custom-radio">
              <input type="radio" id="customRadio1" name="transport" value="aucun" class="custom-control-input" checked>
              <label class="custom-control-label" for="customRadio1">Aucun</label>
            </div>
            <div class="custom-control custom-radio">
              <input type="radio" id="customRadio2" name="transport" value="normal" class="custom-control-input">
              <label class="custom-control-label" for="customRadio2">Normal</label>

        </div>
        <div class="custom-control custom-radio">
              <input type="radio" id="customRadio3" name="transport" value="adapte" class="custom-control-input">
              <label class="custom-control-label" for="customRadio3">Adapté</label>

        </div>
        </div>
        </div>

        @if(isset($erreur))
           <h1>{{$erreur}}</h1>
        @endif
        <div class="col-sm-4">
        <button type="submit"class="btn btn-success">Candidader</button>
        </div>
    </form>
@endsection

@section('js')
<script src="{{ url('/')}}/js/candidater.js"></script>
@endsection
