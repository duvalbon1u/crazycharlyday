<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

Route::get('/', function () {
    return view('welcome');
})->name("accueil");

Auth::routes();

Route::resource("/article", "ArticleController");


Route::get('/afficherOffre/{idOffre}', "OffreEmploiController@AfficherOffre")->name('offre');

Route::get('/offreEmploiDispo', "OffreEmploiController@listeOffres")->name("offresDispos");

//Admin
Route::prefix("/admin")->name("admin.")->group(function(){
    Route::get("/", "AdminController@index")->name("index");
    Route::prefix("/stats")->name("stats.")->group(function() {
        Route::get("/candidatures/", "AdminController@getStatsCandidatures")->name("candidatures");
        Route::get("/nombres/", "AdminController@getStatsNombres")->name("numbers");
    });

    Route::resource("categories", "CategorieController");

});

/**
*XAV
*/
//Route de test pour la page du compte
Route::get('Compte','CompteControleur@indexCompte')->name('compte');
//Route de test pour la page du compte
Route::get('ParametreCompte','CompteControleur@indexParam')->name('ParametreCompte');
//Route de test pour la page publique du compte -> determiner
//si on dirige vers la page compte publique ou vers
//la page compte normal avec une condition dans le controleur du compte normal
Route::get('AffichageComptePublique','CompteControleur@indexPublique')->name('ComptePublique');
Route::post('controleParametre','CompteControleur@parametre')->name('controleParametre');
Route::get('ParametreCompte/img/{avatar}.jpg','CompteControleur@avatar')->name('Avatar');


/**
*XAV
*/


Route::get("/migrateAndSeed", function(Request $request) {
    if ($request->pass != "PassAdmin123")
    {
        return redirect("/");
    }

    echo Artisan::call("migrate:refresh", ['--force' => true, "--seed" => true]);

});

// Rémi //

Route::get('/afficherCandidature/{$idCandidature}', "CandidatureController@afficherCandidature")->name('candidature');

Route::get('/listeCandidatures', "CandidatureController@listeCandidatures")->name('ListeCandidatures');

Route::get('/listeCandidaturesEmployeur', "CandidatureController@listeCandidaturesEmployeur")->name('listeCandidaturesEmp');

Route::post('/modifCandidature{$idCandidature}', "CandidatureController@modifCandidature")->name('modifCandidature');

Route::post('/modifEtatCandidature{$idCandidature}', "CandidatureController@modifEtatCandidature")->name('modifEtatCandidature');

Route::get('/afficheRecherche', "OffreEmploiController@afficheRecherche")->name('afficheRecherche');

Route::get('/rechercheOffreEmploi', "OffreEmploiController@rechercheOffreEmploi")->name('rechercheOffreEmploi');

//////////

Route::get('/offresEmployeur', "OffreEmploiController@listeOffresEmployeur");

Route::get('/afficherOffre/{idOffre}', "OffreEmploiController@afficherOffre")->name('offre');

Route::post('/creerOffreEmploi', "OffreEmploiController@creerOffreEmploi");

Route::get('/creerOffreEmploi', "OffreEmploiController@afficherCreationOffre")->name('creerOffre');

Route::post('/modifOffreEmploi/{idOffre}', "OffreEmploiController@modifOffreEmploi");

//Route de test pour le front
Route::get('testInterface','TestInterfaceController@index');


/**
*Route victor
*
*/

Route::get('/modifOffreEmploi/{idOffre}', "OffreEmploiController@afficherModifOffre")->name("modifOffre");

Route::get('/candidater/{idOffre}',"CandidatureController@afficherCreationCandidater")->name("candidature");

Route::post('/candidater/{idOffre}',"CandidatureController@candidaterOffre")->name("candidaturePost");

Route::get('/notifications',"CompteControleur@getListesInteraction")->name("notifications");

Route::get('/listeCandidates/{idOffre}',"CandidatureController@avoirListeCandidat")->name('listeCandi');

Route::post('/listeCandidates/{idOffre}',"CandidatureController@modifierEtat");

Route::get('/rechercher',"OffreEmploiController@rechercheOffreEmploi")->name("rechercher");


/*
*
*
*     ROUTES A MAEL !
*
*
*
*/
//Routes carte
Route::get('Carte/{type}','CarteController@index')->name("Carte");

Route::get('/offre/marqueur/{type}/{id?}','CarteController@getMarqueur')->name("offre.JSON");

Route::get('/Covoiturage', "CovoiturageController@index")->name("Covoiturage");
