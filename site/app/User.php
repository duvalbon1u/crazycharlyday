<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom', 'prenom', 'email', 'password', 'adressePro', 'adressePerso'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, "user_roles");
    }

    public function hasRole($role) : boolean
    {
        return $this->roles->contains(function ($e) {
            return $role instanceof Role ? $e->nom == $role->nom : $e->nom == $role;
        });
    }

    public function offres(){
        return $this->hasMany(OffreEmploi::class, "employeur_id");
    }

    public function candidatures()
    {
        return $this->hasMany(Candidature::class);
    }
}
