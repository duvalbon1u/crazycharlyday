<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropositionTransport extends Model
{
    protected $fillable = ['besoin_id', 'chauffeur_id', 'candidature_id', 'etat'];

    public function candidature()
    {
        return $this->belongsTo(Candidature::class);
    }

    public function besoin()
    {
        return $this->belongsTo(BesoinTransport::class, "besoin_id");
    }

    public function chauffeur()
    {
        return $this->belongsTo(User::class, "chauffeur_id");
    }
}
