<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BesoinTransport extends Model
{
    protected $fillable = ['candidature_id'];

    public function candidature()
    {
        return $this->belongsTo(Candidature::class);
    }

    public function propositions()
    {
        return $this->hasMany(PropositionTransport::class, "besoin_id");
    }
}
