<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategorieActivite extends Model
{
    //
    protected $table = "categories";

    protected $fillable = ['nom'];

    public $timestamps = false;
}
