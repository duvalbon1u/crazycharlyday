<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestInterfaceController extends Controller
{
    public function index(){
        //$tab = [];
        $tab = [1,2,3,4,5,6,7,8,9];
        return view("candidater",['titre' => "LeTest",'nombres'=>$tab]);
    }
}
