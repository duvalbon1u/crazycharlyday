<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OffreEmploi;
use App\BesoinTransport;
use App\Candidature;

class CarteController extends Controller
{

  public function index($type){
    if($type!="Emploi"&&$type!="Covoiturage")
      abort(404);
    return view("carte",['titre' => "Carte", "type" =>$type]);
  }

  public function getMarqueur($type, $id=null){
    $offre = new \Illuminate\Support\Collection();
      if($type==="Emploi"){
        if($id)
          $offre->push(OffreEmploi::find($id));
        else
          $offre = OffreEmploi::all()->unique(['lieu']);
        $url=function($o) {return route("offre", $o);};
      }
      else if($type==="Covoiturage"){
          $offre = BesoinTransport::all();
          $offre = $offre->map(function($e) {return $e->candidature;});
          $url=function($o) {return route('accueil');};
      }
      else{
        abort(404);
      }
    $offre = $offre->map(function($e) use($type, $url) {
      $e->type=$type;
      $e->url=$url($e);
      return $e;
    });
    return response()->json($offre->values()->all());
  }
}
