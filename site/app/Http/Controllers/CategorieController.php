<?php

namespace App\Http\Controllers;

use App\CategorieActivite;
use App\OffreEmploi;
use Illuminate\Http\Request;

class CategorieController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "nom" => "required|string|unique:categories|max:255"
        ]);

        $categorie = new CategorieActivite();
        $categorie->nom = $request->input('nom');

        if ($categorie->save())
            return redirect()->route("admin.index")->with("message", "Catégorie créée avec succès");
        else
            return redirect()->route("admin.index")->with("erreur", "Erreur à la création");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategorieActivite  $categorieActivite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategorieActivite $categorieActivite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CategorieActivite  $categorieActivite
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $categorieActivite = CategorieActivite::find($id);
        if (OffreEmploi::all()->contains(function($e) use ($categorieActivite){return $e->categorie->id === $categorieActivite->id;}))
            return redirect()->route("admin.index")->with("erreur", "La catégorie ne peut pas être supprimée car elle est utilisée par des offres d'emplois");

        if ($categorieActivite->delete())
            return redirect()->route("admin.index")->with("message", "Catégorie supprimée");

        return redirect()->route("admin.index")->with("erreur", "La catégorie n'existe pas");
    }
}
