<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\OffreEmploi;
use App\Candidature;
use Illuminate\Http\Request;

class CandidatureController extends Controller{
    
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function afficherCandidature($idCandidature){
        $candidature = Candidature::where('id', '=', $idCandidature)->first();
        $titre = "Candidature à une offre d'emploi";
        
        if(isset($candidature)){
            // Revoir le nom
            return view('candidature', compact('candidature', 'titre'));
        } else{
            $erreur = 'Offre inexistante';
            return view('candidature', compact('erreur', 'titre'));
        }
    }
    
    public function afficherCreationCandidater($idOffre){
        $offre = OffreEmploi::where('id', '=', $idOffre)->where('etat', 'like', 'actif')->where('date_expiration', '>', now())->first();
        $titre = "Candidature à une offre";
        $adresse = Auth::user()->adressePerso??Auth::user()->adressePro;
        if(isset($offre)){
            return view('candidater', compact('offre', 'titre','adresse'));
        } else{
            $erreur = 'Offre inexistante';
            return view('candidater', compact('erreur', 'titre'));
        }
    }
    
    public function candidaterOffre($idOffre){
        $adresse = Auth::user()->adressePerso??Auth::user()->adressePro;
        $offre = OffreEmploi::where('id', '=', $idOffre)->where('etat', 'like', 'actif')->where('date_expiration', '>', now())->where('employeur_id', '!=', Auth::id())->first();
        $peutCandidater = !$offre->candidatures->contains(function($c) {
           return $c->user->id === Auth::id(); 
        });
        if($peutCandidater){
            if(isset($offre)){
               
                $c = new candidature();
                $c->offre_id = $idOffre;
                $c->user_id = Auth::id();
                $c->etat = 'attente';

                
                if(isset($_POST['profil']))
                     $c->depart = Auth::user()->adressePerso;
                else
                    $c->depart = $_POST['adressePerso'];

                if(isset($_POST['typeTransport']))
                    $c->typeTransport = $_POST['typeTransport'];
                else
                    $c->typeTransport = 'aucun';
                 
                try{
                   $c->save(); 
                } catch(Exception $e){
                    $titre = 'Candidature à une offre d\'emploi';
                    $erreur = 'Une erreur est survenue lors de la sauvegarde de la candidature';
                    return view('candidater', compact('adresse','offre','erreur', 'titre'));
                }
            }
        } else{
            $titre = 'Candidature à une offre d\'emploi';
            $erreur = 'Vous ne pouvez pas candidater à cette offre';
            return view('candidater', compact('adresse','offre','erreur', 'titre'));
        }
        return redirect()->route('notifications');
    }
    
    public function listeCandidatures(){
        $candidatures = Auth::user()->candidatures;
        
        
        $titre = "Liste de vos candidatures";
        
        // Revoir le nom
        return view('candidatures', compact('candidatures', 'titre'));
    }
    
    public function listeCandidaturesEmployeur(){
        $candidatures = Auth::user()->offres()->candidatures;
        // Requête à revoir
        
        
        $titre = "Liste des candidatures pour ce poste";
        
        // Revoir le nom
        return view('candidaturesEmployeur', compact('candidatures', 'titre'));
    }
    
    public function modifCandidature($idCandidature){
        $candidature = Candidature::where('id', '=', $idCandidature)->first();
        
        if(isset($_POST['depart']))
            $candidature->depart = $_POST['depart'];

        if(isset($_POST['typeTransport']))
            $candidature->typeTransport = $_POST['typeTransport'];
        try{
            $candidature->save();
        } catch(Exception $e){
            $titre = 'Candidature à une offre d\'emploi';
            $erreur = 'Une erreur est survenue lors de la mdification de votre candidature';
            return view('candidater', compact('erreur', 'titre'));
        }
        
        
        return redirect()->route('notifications');
        
    }
    
    public function modifEtatCandidature($idCandidature){
        $candidature = Candidature::where('id', '=', $idCandidature)->first();
        
        if(isset($_POST['etat']))
            $candidature->depart = $_POST['etat'];

        try{
            $candidature->save();
        } catch(Exception $e){
            $titre = 'Candidature à une offre d\'emploi';
            $erreur = 'Une erreur est survenue lors de la mdification de votre candidature';
            return view('candidature', compact('erreur', 'titre'));
        }

        redirect()->route('candidature', ['idCandidature' => $idCandidature]);
    }
    
    public function avoirListeCandidat($idOffre){
        $offres = OffreEmploi::find($idOffre)->candidatures;
        $titre="Consulter liste candidat";
        return view('listeCandidat',compact('titre','offres'));   
    }
    
    public function modifierEtat($idOffre){
         $candidature = OffreEmploi::find($idOffre)->candidatures->where('user_id','=',$_POST['idUser'])->first();
        if(isset($_POST['etat'])){
            $candidature->etat = $_POST['etat'];
        }
        $candidature->save();
        $offres = OffreEmploi::find($idOffre)->candidatures;
        $titre="Consulter liste candidat";
        return view('listeCandidat',compact('titre','offres'));   
    }
}
