<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\OffreEmploi;
use App\Candidature;
use App\BesoinTransport;
use Illuminate\Http\Request;

class BesoinTransportController extends Controller{
    
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function creerBesoinTransport($idCandidature){
        $cand = Candidature::where('id', '=', $idCandidature)->first();
        
        $besoin = new BesoinTransport();
        $besoin->idCandidature = $cand->id;
        
        try{
            $besoin->save();
        } catch(Exception $e){
            print $e;
        }
        
        return redirect()->route('notifications');
    }
    
    public function afficherCreationProposition($idBesoin){
        $titre = "Proposition de transport";
        
        $besoin = BesoinTransport::where('id', '=', $idBesoin)->first();
        $cand = Candidature::where('id', '=', $besoin->candidature_id)->first();
        $offre = $cand->offre;
        
        $depart = $cand->depart;
        $typeTransport = $cand->typeTransport;
        $arrive = $offre->lieu;
        
        return view('afficherCreationProposition', compact('depart', 'typeTransport', 'arrivee', 'titre'));
    }
    
    public function creerPropositionTransport($idBesoin){
        $prop = new PropositionTransport();
        $prop->besoin_id = $idBesoin;
        $prop->candidature_id = $_POST['idCandidature'];
        $prop->chauffeur_id = Auth::id();
        $prop->etat = 'attente';
        
        try{
            $prop->save();
        } catch(Exception $e){
            print $e;
        }
        
        return redirect()->route('afficherCreationProposition', ['idBesoin' => $idBesoin]);
    }
    
    public function afficherBesoinTransportChauffeur($idBesoin){
        $titre = "Besoin de transport";
        
        $besoin = BesoinTransport::where('besoin_id', '=', $idBesoin)->where('chauffeur_id', '=', Auth::id())->get();
        
        return view('afficherBesoinTransportChauffeur', compact('besoin', 'titre'));
    }
    
    public function afficherBesoinsTransportChauffeur(){
        $titre = "Besoins de transport";
        
        $besoins = BesoinTransport::where('chauffeur_id', '=', Auth::id())->get();
        
        return view('afficherBesoinsTransportChauffeur', compact('besoins', 'titre'));
    }
    
    public function afficherBesoinsTransportEmploye(){
        $titre "Vos besoins de transport";
        
        $cand = Candidature::where('user_id', '=', Auth::id())->get();
        
        $besoin = BesoinTransport::where('candidature_id', '=', $cand->id)->propositions;
        
        return view('afficherBesoinsTransportEmploye', compact('besoins', 'titre'));
    }
    
    public function afficherPropositionTransportChauffeur(){
        $titre "Proposition des transports";
        
        $req = PropositionTransport::where('chauffeur_id', '=', Auth::id());
        $props = $req->get();
        $user = $req->where('etat', 'like', 'acceptee')->candidature;
        
        return view('afficherPropositionTransportChauffeur', compact('props', 'user', 'titre'));
    }
    
    public function gestionPropositionTransportEmploye($idProposition){
        $prop = PropositionTransport::where('id', '=', $idProposition)->first();
        
        $prop->etat = $_POST['etat'];
        try{
            $prop->save();
        } catch(Exception $e){
            print $e;
        }
        
        return redirect()->route('afficherBesoinTransportChauffeur', ['idBesoin' => $prop->besoin_id]);
    }
}
