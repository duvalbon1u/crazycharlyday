<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OffreEmploi;
use App\BesoinTransport;
use App\Candidature;

class CovoiturageController extends Controller{

  public function index(){
    $article = new \Illuminate\Support\Collection();
    $covoiturage = BesoinTransport::all();
    $article = $covoiturage->map(function($e) {return $e->candidature;} );
    return view("covoiturage",compact('covoiturage'));
  }

}
