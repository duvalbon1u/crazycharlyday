<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\OffreEmploi;
use App\CategorieActivite;
use Illuminate\Http\Request;

class OffreEmploiController extends Controller{

    public function __construct(){
        $this->middleware('auth')->except('afficherOffre', 'listeOffres', 'rechercheOffreEmploi', 'afficheRecherche');
    }

    public function listeOffres(){
        try{
            $offres = OffreEmploi::where('etat', 'like', 'actif')->where('date_expiration', '>', now())->get();

            $titre = "Recherche d'offre d'emploi";

        } catch(Exception $e){
            $erreur = "Liste des offres d'emploi indisponible !";
            return view('offreRecherche', compact('erreur', 'titre'));
        }
        return view('offreRecherche', compact('offres', 'titre'));

    }

    public function listeOffresEmployeur(){
        try{
            $offres = Auth::user()->offres;

            $titre = "Liste des offres d'emploi";
        } catch(Exception $e){
            $erreur = "Liste des offres d'emploi indisponible !";
            return view('listeOffresEmployeur', compact('erreur', 'titre'));
        }

        if(isset($offres)){
            return view('listeOffresEmployeur', compact('offres', 'titre'));
        } else{
            $erreur = "Aucune liste des offres d'emploi disponible !";
            return view('listeOffresEmployeur', compact('erreur', 'titre'));
        }
    }

    public function afficherOffre($idOffre){
        try{
            $offre = OffreEmploi::where('id', '=', $idOffre)->first();
            $titre = "Offre d'emploi : " . $offre->intitule;
        } catch(Exception $e){
            $erreur = "Offre d'emploi indisponible !";
            return view('offre', compact('erreur', 'titre'));
        }

        if(isset($offre)){
            return view('offre', compact('offre', 'titre'));
        } else{
            $erreur = "Offre d'emploi indisponible !";
            return view('offre', compact('erreur', 'titre'));
        }
    }

    public function afficherCreationOffre(){
        try{
            $categories = CategorieActivite::get();

            $titre = "Creer votre offre";
        } catch(Exception $e){
            $titre = "Creer votre offre";
            $erreur = "Catégories indisponible !";
            return view('offre',compact('erreur', 'titre'));
        }

        if(isset($categories)){
            return view('creationOffre', compact('offre', 'titre'));
        } else{
            $erreur = "Catégories indisponible !";
            return view('offre', compact('erreur', 'titre'));
        }
    }

    public function creerOffreEmploi(){
        $offre = new OffreEmploi();
        //die(var_dump($_POST));

        try{
            $offre->employeur_id = Auth::id();
            $offre->categorie_id = $_POST['categorie'];
            $offre->date_expiration = $_POST['expiration'];
            $offre->lieu = $_POST['lieu'];
            $offre->etat = 'actif';
            $offre->intitule = $_POST['intitule'];
            $offre->profilPosteCourt = $_POST['profilPosteCourt'];
            $offre->profilPoste = $_POST['profilPoste'];
        } catch(Exception $e){
            $titre = "Creer votre offre";
            $erreur = "Erreur lors de la saisie de la création de l'offre";
            return view('creationOffre', compact('erreur', 'titre'));
        }


        try{
            $offre->save();
        } catch(Exception $e){
            $titre = "Creer votre offre";
            $erreur = "Erreur de création d'offre d'emploi";
            return view('creationOffre', compact('erreur', 'titre'));
        }

        return redirect()->route('offre',['idOffre'=>$offre->id]);
    }

    public function afficherModifOffre($id){
        $categories = CategorieActivite::get();
        $offre = OffreEmploi::where('id', '=', $id)->first();
        $titre = "Modification de l'offre";
        return view('modifOffreEmploi',compact('titre','offre','categories'));
    }

    public function modifOffreEmploi($idOffre) {

        try{
            $offre = Auth::user()->offres()->where('id', '=', $idOffre)->first();
        } catch(Exception $e){
            $titre = "Offre d'emploi";
            $erreur = "Impossible de trouver l'offre d'emploi";
            return view('offre', compact('erreur', 'titre'));
        }

        if(isset($offre)){
            if(isset($_POST['categorie'])){
                $offre->categorie_id = $_POST['categorie'];
            }

            if(isset($_POST['expiration'])){
                $offre->date_expiration = $_POST['expiration'];
            }

            if(isset($_POST['lieu'])){
                $offre->lieu = $_POST['lieu'];
            }

            if(isset($_POST['intitule'])){
                $offre->intitule = $_POST['intitule'];
            }

            if(isset($_POST['profilPosteCourt'])){
                $offre->profilPosteCourt = $_POST['profilPosteCourt'];
            }

            if(isset($_POST['profilPoste'])){
                $offre->profilPoste = $_POST['profilPoste'];
            }

            if(isset($_POST['etat'])){
                $offre->etat = $_POST['etat'];
            }

            try{
                $offre->save();
            } catch(Exception $e){
                $titre = "Offre d'emploi";
                $erreur = "Erreur de modification d'offre d'emploi";
                return view('offre', compact('erreur', 'titre'));
            }
        }

        return redirect()->route('offre',['idOffre'=>$offre->id]);
    }

    public function afficheRecherche(){
        $titre = "Recherche des offres d'emploi";

        return view('offreAffichageRecherche', compact('titre'));
    }

    public function rechercheOffreEmploi(){
        $title= "rechercher";
        $res = array();
        if(isset($_GET['search'])){

            if(! filter_var($_GET['search'], FILTER_SANITIZE_SPECIAL_CHARS)){
                throw new ExceptionPerso('La valeur entrée n\'est pas valide !', 'avert');
            } else{
                $search = filter_var($_GET['search'], FILTER_SANITIZE_SPECIAL_CHARS);
            }

            if(isset($_GET['lieu'])){
                if(isset($_GET['etat']) && ($_GET['etat'] == 'inactif')){
                    $res = OffreEmploi::where('lieu', 'like', '%'.$search.'%' )->where('etat', 'like', 'inactif')->where('date_expiration', '>', now())->get();
                } else{
                    $res = OffreEmploi::where('lieu', 'like', '%'.$search.'%' )->where('etat', 'like', 'actif')->where('date_expiration', '>', now())->get();
                }

            } else if(isset($_GET['deep'])){
                if(isset($_GET['etat']) && ($_GET['etat'] == 'inactif')){
                    $res = OffreEmploi::where('etat', 'like', 'inactif')->where('date_expiration', '>', now())->where(function($q) use($search){
                        $q->where("profilPosteCourt", "like", "%" . $search . "%")->orwhere("profilPoste", "like", "%" . $search . "%")->orwhere("intitule", "like", "%" . $search . "%");
                    })->get();
                } else{
                    $res = OffreEmploi::where('etat', 'like', 'actif')->where('date_expiration', '>', now())->where(function($q) use($search){
                        $q->where("profilPosteCourt", "like", "%" . $search . "%")->orwhere("profilPoste", "like", "%" . $search . "%")->orwhere("intitule", "like", "%" . $search . "%");
                    })->get();
                }

            } else{
                if(isset($_GET['etat']) && ($_GET['etat'] == 'inactif')){
                    $res = OffreEmploi::where('etat', 'like', 'inactif')->where('date_expiration', '>', now())->where(function($q) use($search){
                        $q->where("profilPosteCourt", "like", "%" . $search . "%")->orwhere("intitule", "like", "%" . $search . "%");
                    })->get();
                } else{
                    $res = OffreEmploi::where('etat', 'like', 'actif')->where('date_expiration', '>', now())->where(function($q) use($search){
                        $q->where("profilPosteCourt", "like", "%" . $search . "%")->orwhere("intitule", "like", "%" . $search . "%");
                    })->get();
                }
            }


        }
        return view('rechercher',compact('title','res'));
    }
}
