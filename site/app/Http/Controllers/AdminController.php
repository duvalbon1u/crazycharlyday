<?php

namespace App\Http\Controllers;

use App\Candidature;
use App\OffreEmploi;
use App\User;
use App\PropositionTransport;
use App\CategorieActivite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    protected $noms = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];

    public function __construct() {
        $this->middleware("auth");
        $this->middleware("can:super_admin");
    }

    public function index()
    {
        $user = Auth::user();
        $categories = CategorieActivite::all();
        return view("admin.index", compact("user", "categories"));
    }

    protected function getMonths(): array{
        $months = [];

        $now = new \DateTime();
        $m = intval($now->format('m')) + 1;
        $a = intval($now->format('Y'));
        if ($m > 12)
        {
            $m = 1;
            $a++;
        }

        for ($i = 11; $i >=0; $i--, $m--)
        {
            if ($m < 1)
            {
                $m = 12;
                $a--;
            }
            array_unshift($months, [
                'num' => $m,
                'annee' => $a
            ]);
        }
        return $months;
    }

    public function getStatsCandidatures()
    {

        $candidatures = Candidature::all();
        $offres = OffreEmploi::all();

        $stats = ["candidatures" => [], "offres" => []];

        $months = $this->getMonths();

        $stats['labels'] = \array_map(function($m) {return $this->noms[$m['num'] - 1] . " " . $m['annee'];}, $months);
        $lastD = new \DateTime("1970-01-01");
        foreach ($months as $m)
        {
            $d = new \DateTime("$m[annee]-$m[num]-01");
            $filter = function($e) use ($lastD, $d) {return $e->created_at >= $lastD && $e->created_at <= $d;};
            $stats["candidatures"][] = $candidatures->filter($filter)->count();
            $stats['offres'][] = $offres->filter($filter)->count();
            $lastD = $d;
        }

        return response()->json($stats);
    }

    public function getStatsNombres()
    {

        $stats = [
            'users' => User::count(),
            'candidatures' => Candidature::count(),
            'offres' => OffreEmploi::count(),
            'candidaturesAcceptees' => Candidature::where("etat", "=", "accepte")->count(),
            'covoits' => PropositionTransport::where("etat", "=", "accepte")->count(),
            "employeur" => OffreEmploi::distinct("employeur_id")->count()
        ];

        return response()->json($stats);
    }
}
