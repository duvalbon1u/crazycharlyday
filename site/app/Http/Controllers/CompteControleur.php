<?php

namespace App\Http\Controllers;

use App\User;
use App\Candidature;
use App\OffreEmploi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompteControleur extends Controller
{

  public function __construct(){
    $this->middleware("auth");
  }

  public function indexParam(){
    //retourne l'utilisateur authentifier
    $usr=Auth::user();
    $tab=array();
    for($i=1;$i<=12;$i++){
      array_push($tab,"/img/".$i.".jpg");
    }
  return view("compteParam",['titre' => "Parametre de Mon Compte",'usr'=>$usr,'tab'=>$tab]);
  }

  public function indexCompte(){
    //on recherche le compte qu'on va afficher
    $usr=Auth::user();
    $tab = [1,2,3];
    return view("compte",['titre' => "MonCompte",'nombres'=>$tab,'usr'=>$usr]);
  }

    public function getListesInteraction(){
        $candidature = Candidature::where('user_id','=',Auth::id())->get();
        $offreEmple = OffreEmploi::where('employeur_id','=',Auth::id())->get();
        $titre = "Mes notifications";
        return view("notificationsCompte",compact('titre','candidature','offreEmple'));
    }

    public function parametre(){
      $usr=Auth::user();
      echo $usr->prenom."</br>";
      echo $usr->nom."</br>";
      echo $usr->email."</br>";
      echo $usr->adressePro."</br>";
      echo $usr->adressePerso."</br>";
      echo "</br>"."</br>";



      //echo $_POST['usrNom'];
      //recuperation des variable Post
      if(isset($_POST['usrPrenom']) && $_POST['usrPrenom']!="" && $_POST['usrPrenom']!=$usr->prenom ){
        $usr->prenom=$_POST['usrPrenom'];
      }
      if(isset($_POST['usrNom']) && $_POST['usrNom']!="" &&  $_POST['usrNom']!=$usr->nom ){
        $usr->nom=$_POST['usrNom'];
      }
      if(isset($_POST['usrEmail']) && $_POST['usrEmail']!="" && $_POST['usrEmail']!=$usr->email ){
        $usr->email=$_POST['usrEmail'];
      }
      if(isset($_POST['usrAdressePro']) && $_POST['usrAdressePro']!="" && $_POST['usrAdressePro']!=$usr->adressePro ){
        $usr->adressePro=$_POST['usrAdressePro'];
      }
      if(isset($_POST['usrAdressePerso']) && $_POST['usrAdressePerso']!="" && $_POST['usrAdressePerso']!=$usr->adressePerso ){
        $usr->adressePerso=$_POST['usrAdressePerso'];
      }

      $usr->save();
    }

    public function avatar($avatar){
      $usr=Auth::user();
      $usr->avatar=$avatar;
      $usr->save();
      return redirect()->route('ParametreCompte');
    }

    public function indexPublique(){
      echo "publique";
    }

}
