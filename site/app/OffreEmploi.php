<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class OffreEmploi extends Model
{
    protected $table = 'offre_emplois';
    protected $fillable = [
       'categorie_id', 'expiration', 'lieu', 'intitule', 'profilPosteCourt', 'profilPoste'
    ];

    public function employeur(){
        return $this->belongsTo(User::class, 'employeur_id');
    }

    public function categorie(){
        return $this->belongsTo(CategorieActivite::class, 'categorie_id');
    }

    public function candidatures(){
        return $this->hasMany(Candidature::class,'offre_id');
    }
    
    public function getDateExpirationAttribute($date)
    {
        return Carbon::parse($date);
    }
}
