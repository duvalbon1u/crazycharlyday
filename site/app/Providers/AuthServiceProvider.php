<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use App\Article;
use App\Policies\ArticlePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Article::class => ArticlePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //authorization here
        Gate::define("super_admin", function(\App\User $user){
            return $user->roles->contains("nom", "admin");
        });
    }
}
