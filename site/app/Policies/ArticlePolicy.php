<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function update(Article $article, User $user)
    {
        return $article->user->id === $article->id;
    }

    public function destroy(User $user)
    {
        return $article->user->id === $article->id;
    }
}
