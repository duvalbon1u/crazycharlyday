<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Candidature extends Model
{
    protected $table = 'candidatures';

    protected $fillable = ['offre_id', 'user_id', 'etat', 'depart', 'typeTransport'];

    public function offre()
    {
        return $this->belongsTo(OffreEmploi::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function getDepartAttribute($depart){
        return $depart??$this->user->adressePerso;
    }
    
     public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date);
    }
}
