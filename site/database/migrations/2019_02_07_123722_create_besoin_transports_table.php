<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBesoinTransportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('besoin_transports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidature_id')->unsigned();
            $table->timestamps();

            $table->foreign('candidature_id')->references('id')->on('candidatures');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('besoin_transports', function($table)
        {
            $table->dropForeign(['candidature_id']);
            $table->dropColumn('candidature_id');
        });
        Schema::dropIfExists('besoin_transports');
    }
}
