<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidatures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offre_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->enum("etat", ['attente', 'acceptee', 'refusee']);
            $table->string("depart")->nullable();
            $table->enum("typeTransport", ["aucun", "normal", "adapte"]);
            $table->timestamps();

            $table->foreign('offre_id')->references('id')->on('offre_emplois');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidatures', function($table)
        {
            $table->dropForeign(['offre_id']);
            $table->dropColumn('offre_id');
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
        });
        Schema::dropIfExists('candidatures');
    }
}
