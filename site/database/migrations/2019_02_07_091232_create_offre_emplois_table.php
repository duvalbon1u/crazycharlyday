<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffreEmploisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offre_emplois', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employeur_id')->unsigned();
            $table->integer('categorie_id')->unsigned();
            $table->timestamp('date_expiration');
            $table->string('lieu');
            $table->enum('etat', ['actif', 'inactif']);
            $table->string('intitule');
            $table->string('profilPosteCourt');
            $table->string('profilPoste', 2048);

            $table->foreign('employeur_id')->references("id")->on("users");
            $table->foreign('categorie_id')->references('id')->on("categories");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offre_emplois', function($table)
        {
            $table->dropForeign(['employeur_id']);
            $table->dropColumn('employeur_id');
            $table->dropForeign(['categorie_id']);
            $table->dropColumn('categorie_id');
        });
        Schema::dropIfExists('offre_emplois');
    }
}
