<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropositionTransportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposition_transports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('besoin_id')->unsigned();
            $table->integer('candidature_id')->unsigned();
            $table->integer('chauffeur_id')->unsigned();
            $table->enum('etat', ['attente', 'acceptee', 'refusee']);
            $table->timestamps();

            $table->foreign('candidature_id')->references('id')->on('candidatures');
            $table->foreign('besoin_id')->references('id')->on('besoin_transports');
            $table->foreign('chauffeur_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposition_transports', function($table)
        {
            $table->dropForeign(['candidature_id']);
            $table->dropColumn('candidature_id');
            $table->dropForeign(['besoin_id']);
            $table->dropColumn('besoin_id');
            $table->dropForeign(['chauffeur_id']);
            $table->dropColumn('chauffeur_id');
        });
        Schema::dropIfExists('proposition_transports');
    }
}
