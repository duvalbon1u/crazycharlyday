<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $pro = $faker->optional()->address;
    $perso = $pro ? $faker->optional()->address : $faker->address;
    return [
        'nom' => $faker->lastName,
        'prenom' => $faker->firstName,
        'email' => $faker->unique()->email,
        'adressePro' => $pro,
        'adressePerso' => $perso,
        'password' => password_hash(/*$faker->regexify('[A-Z0-9._%+-]{8, 12}')*/'123456', PASSWORD_DEFAULT), // secret
        'remember_token' => str_random(16),
        'avatar' => $faker->numberBetween(1,12)
    ];
});
