<?php

use Faker\Generator as Faker;

$factory->define(App\Candidature::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 50),
        'offre_id' => $faker->numberBetween(0, 50),
        "etat" => $faker->randomElement(['attente', 'acceptee', 'refusee']),
        "typeTransport" => $faker->randomElement(["aucun", "normal", "adapte"])
    ];
});
