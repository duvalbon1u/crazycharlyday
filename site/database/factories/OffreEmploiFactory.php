<?php

use Faker\Generator as Faker;

function randomAdresse() : string
{
    $adresses = Storage::get("adresses.json");
    $elements = explode("\n", $adresses);
    $index = random_int(0, count($elements));
    $addr = json_decode($elements[$index]);
    return $addr->name . " " . $addr->postcode;
}

$factory->define(App\OffreEmploi::class, function (Faker $faker) {
    $texte = $faker->text(1200);
    return [
        'employeur_id' => 0,
        'categorie_id' => App\CategorieActivite::all()->random()->id,
        'date_expiration' => $faker->dateTimeBetween("now", "+1 years"),
        'lieu' => randomAdresse($faker),
        'etat' => $faker->randomElement(['actif', 'inactif']),
        'intitule' => $faker->sentence(5),
        'profilPosteCourt' => substr($texte, 0, 100),
        'profilPoste' => $texte
    ];
});
