<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\User::class, 1)->create(['email' => "admin@admin.com"]);
        DB::table('user_roles')->insert([
            'role_id' => "1",
            'user_id' => "1"
        ]);

        factory(App\User::class, 50)->create()->each(function ($u) {
            $u->offres()->save(
                factory(App\OffreEmploi::class)->make()
            );
        });

        factory(App\Candidature::class, 10)->create();


    }
}
