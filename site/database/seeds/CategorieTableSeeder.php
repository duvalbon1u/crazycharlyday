<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("categories")->insert([
            ["nom" => 'Administratif : assistant comptable'],
            ["nom" => 'Administratif : comptable'],
            ["nom" => 'Administratif : secrétaire'],
            ["nom" => 'Administratif : standardiste'],
            ["nom" => 'Bâtiment/Travaux Publics : conducteur d\'engin'],
            ["nom" => 'Bâtiment/Travaux Publics : manœuvre'],
            ["nom" => 'Bâtiment/Travaux Publics : maçon'],
            ["nom" => 'Bâtiment/Travaux Publics : électricien'],
            ["nom" => 'Commerce et vente : assistant commercial'],
            ["nom" => 'Commerce et vente : commercial'],
            ["nom" => 'Commerce et vente : manager'],
            ["nom" => 'Commerce et vente : vendeur polyvalent'],
            ["nom" => 'Logistique : magasinier'],
            ["nom" => 'Logistique : préparateur de commandes'],
            ["nom" => 'Restauration et hôtellerie : aide cuisinier'],
            ["nom" => 'Restauration et hôtellerie : cuisinier'],
            ["nom" => 'Restauration et hôtellerie : employé polyvalent'],
            ["nom" => 'Restauration et hôtellerie : serveur'],
            ["nom" => 'Transport : cariste'],
            ["nom" => 'Transport : chauffeur de bus'],
            ["nom" => 'Transport : conducteur poids lourd'],
            ["nom" => 'Transport : livreur']
        ]);
    }
}
