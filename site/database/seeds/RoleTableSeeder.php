<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'nom' => "admin",
        ]);
        DB::table('roles')->insert([
            'nom' => "utilisateur",
        ]);
        DB::table('roles')->insert([
            'nom' => "anonyme",
        ]);
    }
}
