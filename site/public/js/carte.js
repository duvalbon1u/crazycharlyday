// On initialise la latitude et la longitude de Paris (centre de la carte)
var lat = 46;
var lon = 3;
var macarte = null;
let contract = L.icon({
    iconUrl: '/www/duvalbon1u/img/contract.png',
  //  shadowUrl: 'leaf-shadow.png',
    iconSize:     [30, 35], // size of the icon
  //  shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [15, 15], // point of the icon which will correspond to marker's location
  //  shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
});
// Fonction d'initialisation de la carte
function initMap() {
  // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
          macarte = L.map('map').setView([lat, lon], 5);
          // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
          L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
              // Il est toujours bien de laisser le lien vers la source des données
              //attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
              minZoom: 1,
              maxZoom: 20
          }).addTo(macarte);
      }
window.onload = function(){
  // Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
  initMap();
};
let map = $("#map");
fetch(map.data("fetch-url"))
.then(data=>data.json())
  .then(offres=> {
      offres.forEach(offre=> {
    $.get(location.protocol + '//nominatim.openstreetmap.org/search?format=json&q='+offre.lieu, function(data){
      let pop =  offre.intitule;
      if(pop.length>20){
        pop = pop.substring(0,20)+" ...";
      }
      pop="<a href='"+offre.url+"'>"+pop+"</a>";
      try{
        let marker = L.marker([data[0]['lat'], data[0]['lon']]);
        marker.bindPopup(pop);
        marker.addTo(macarte);
        if(offre.type==="Emploi")
          marker.setIcon(contract);
      }catch(error){};
    });
});})
.catch();
