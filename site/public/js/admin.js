$(document).ready(() => {

    fetch($("#offres-candidatures").data("fetch-url"))
    .then(data => data.json())
    .then(stats => {
        loadCandidatureOffresStats(stats);
    });

    fetch($("#numberStats").data("fetch-url"))
    .then(data => data.json())
    .then(stats => {
        console.log(stats);
        console.log(loadNumbersStats);
        loadNumbersStats(stats);
    })
    .catch(e => console.error(e));

});

function animateNumber(elem, begin, end, time = 2000, easing="swing")
{
    let step = end - begin;
    elem.text(begin);
    console.log(time);
    elem.animate({color: 'black'}, {
        duration : time,
        easing : easing,
        progress: function(p, progress, remaining) {
            elem.text(Math.round(begin + step * progress));
        },
        complete: function(){elem.text(end);}
    });
}

function loadNumbersStats(stats){
    console.log("1");
    let elems = [
        {elem : $("#nbUsers"), nom: 'users'},
        {elem : $("#nbCandidatures"), nom: 'candidatures'},
        {elem : $("#nbOffresEmplois"), nom: 'offres'},
        {elem : $("#nbCandidaturesAcceptees"), nom: 'candidaturesAcceptees'},
        {elem : $("#nbCovoiturage"), nom: 'covoits'},
        {elem : $("#nbEmployeurs"), nom: 'employeur'}
    ];

    for (let e of elems)
    {
        animateNumber(e.elem, 0, stats[e.nom]);
    }
}


function loadCandidatureOffresStats(stats){
    labels = stats.labels;
    nbOffresEmplois = stats.offres;
    nbCandidatures = stats.candidatures;
    let chart = new Chart($("#offres-candidatures")[0].getContext('2d'), {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: [{
                label: "Offres d'emplois créées",
                backgroundColor: 'transparent',
                borderColor: 'rgb(255, 0, 0)',
                data: nbOffresEmplois,
            },
            {
                label: "Candidatures soumises",
                backgroundColor: 'transparent',
                borderColor: 'rgb(0, 0, 255)',
                data: nbCandidatures,
            }]
        },

        // Configuration options go here
        options: {responsive : true}
    });
}
